function draw(description, data) {
  // Look at configuration parameters
  var configuration = description.general;
  var groups = description.datapoints.groups;

  // Get the data dimensions
  data = data.datapoints.groups;
	var n = data[0].series.length;
  var m = data.length;

  var yDomain = configuration.domains.y;
	var maxY = yDomain.max;
	var minY = yDomain.min;

  var title         = configuration.title;
  var xLabels       = configuration.domains.x.series || [];
  var xLabel        = configuration.domains.x.name;
  var yLabel        = configuration.domains.y.name;
  var legend_labels = groups.map(function(d) { return d.name; });

  // Sensible defaults
  if (xLabels === undefined) {
    xLabels = [];
  }

  // Fill in x-series to at least cover the range of values available
  if (xLabels.length < n) {
    xLabels = xLabels.concat(d3.range(xLabels.length, n).map(function(e) {
      return {"name": e};
    }));
  }

  // Initialize margins
	var margin = {top: 60, right: 40, bottom: 30, left: 40};
  var height = description.height - margin.top  - margin.bottom;

	var y = d3.scale.linear()  //  y axis
    .range([height, 0])
    .domain([minY, maxY]);

	var z = d3.scale.category10();

	var yAxis = d3.svg.axis()
    .scale(y)
    .orient("left");

	var tip = d3.tip()
	.attr('class', 'd3-tip')
	.offset([-10, 0])
	.html(function(d,i) {
		return "<strong>" + xLabels[i].name + ":</strong> <span style='color:red'>" + d + "</span>";
	});

  var svg = d3.select("body").append("svg")
    .attr("width",  description.width)
    .attr("height", description.height)
    .append("g")
      .attr("transform", "translate(" + margin.left + "," + margin.top + ")");

	svg.call(tip);

	var graph = svg.append("g")
    .attr("class", "graph")
    .selectAll("g")
      .data(data)
    .enter()
    .append("g")
      .attr('id', function(d, i) { return "groups-" + i; });

  graph.append("text")
    .attr("class", "names")
    .attr('id', function(d, i) { return "names-" + i; })
    .attr("transform", function(d) { return "translate(" + (0) + "," + y(d.series[d.series.length-1]) + ")"; })
    .attr("x", 4)
    .attr("dy", ".35em")
    .style({
      opacity: 0.5,
      fill: configuration.textColor
    })
    .text(function(d,i) { return legend_labels[i]; });

  // Determine right-hand margin
  var maxWidth = 0;
  for (var i = 0; i < groups.length; i++) {
    var element = document.getElementById("names-" + i);
    var textWidth = element.getComputedTextLength();
    if (maxWidth < textWidth) {
      maxWidth = textWidth;
    }
  }

  margin.right += maxWidth;

  // Determine size
  var width  = description.width - margin.left - margin.right;

  for (var i = 0; i < groups.length; i++) {
    var element = document.getElementById("names-" + i);

    d3.select(element)
      .attr('transform', function(d) {
        return "translate(" + (width) + "," + y(d.series[d.series.length-1]) + ")";
      });
  }

  // Set up X domain
	var x0 = d3.scale.ordinal()
    .domain(d3.range(0, n))
    .rangePoints([20, width-20]);

	var xAxis = d3.svg.axis()
    .scale(x0)
    .tickFormat(function(i) {
      return xLabels[i].name;
    })
    .orient("bottom");

	// where the line gets its properties, how it will be interpolated
  var line = d3.svg.line()
    .interpolate("monotone")
    .x(function(d,i) { return x0(i); })
    .y(function(d) { return y(d); })
    ;

  d3.selection.prototype.moveToFront = function() {
    return this.each(function(){
      this.parentNode.appendChild(this);
    });
  };

	graph.append("path")
    .attr("class", "line")
    .attr('data-index', function(d, i) { return i; })
    .attr("d", function(d,i) { return line(d.series, i); })
    .style({
      "stroke": function(d, i) { return groups[i].color; },
      "fill": "none"
    })
    .on("mouseover", function(d,i) {
      var self = d3.select(this);

      // Selects the line
      self.style("opacity", "1");

      // Highlight the name
      var index = self.attr('data-index');
      var nameElement = d3.select('#names-' + index);
      nameElement.style({
        opacity: 1.0,
        "font-weight": "bold",
        fill: groups[parseInt(index)].color
      });

      // Bring line and name to the front
      d3.select('#groups-' + index).moveToFront();
    })
    .on("mouseout", function(d,i) {
      var self = d3.select(this);

      self.style("opacity", "");

      // Highlight the name
      var index = self.attr('data-index');
      var nameElement = d3.select('#names-' + index);
      nameElement.style({
        opacity: 0.5,
        "font-weight": "normal",
        fill: configuration.textColor
      });
    })
    ;

	svg.append("g")
      .attr("class", "x axis")
      .attr("transform", "translate(0," + height + ")")
      .call(xAxis)
      .append("text")
        .attr("x", (width / 2))
        .attr("y", 30)
        .attr("text-anchor", "middle")
        .style({
          "fill": configuration.domains.x.textColor,
          "font-size": "12px"
        })
        .text(xLabel);

  // Render Y Axis
	var yAxisG = svg.append("g")
    .attr("class", "y axis")
    .call(yAxis);

	yAxisG.append("text")
      .attr("transform", "rotate(-90)")
      .attr("y", 6)
      .attr("dy", ".71em")
      .style({
        "fill": configuration.domains.y.textColor,
        "text-anchor": "end"
      })
      .text(yLabel);

  /* Minor ticks */
  if (configuration.domains.y.minorStep > 0) {
    yAxisG.selectAll('line')
          .data(d3.range(yDomain.min, yDomain.max+1, yDomain.minorStep), function(d) { return d;})
          .enter()
          .append('line')
          .attr('class', 'minor')
          .attr('y1', y)
          .attr('y2', y)
          .attr('x1', 0)
          .attr('x2', -8)
          .style({
            "stroke-width": "1px",
            "stroke": yDomain.minorTickColor
          });
  }

	svg.append("text")
    .attr("x", (width / 2))
    .attr("y", 0 - (margin.top/2))
    .attr("dominant-baseline", "middle")
    .attr("text-anchor", "middle")
    .style("font-size", "18px")
    .style({
      "fill": configuration.titleColor,
      "font-size": "18px"
    })
  .text(title);

	graph.selectAll(".data_point")
    .data(function(d){return d.series;})
    .enter()
    .append('svg:circle')
      .attr('class', 'data_point')
      .attr('data-index', function(d, i, j) { return j; })
      .attr('fill', function(d, i, j) { return groups[j].color; })
      .attr('cx', function(d,i) { return x0(i) + x0.rangeBand()/2; })
      .attr('cy', function(d) { return y(d); })
      .attr('r', function() { return 4; })
      .on("mouseover", function(d,i) {
        var self = d3.select(this);

        // Selects the line
        var line_opacity = this.parentNode.childNodes;
        d3.select(line_opacity[1])
          .style("opacity", "1");

        // Highlight the name
        var index = self.attr('data-index');
        var nameElement = d3.select('#names-' + index);
        nameElement.style({
          opacity: 1.0,
          "font-weight": "bold",
          fill: groups[parseInt(index)].color
        });

        // Updates the radius
        self.attr("r", 8)
            .transition()
            .duration(750);

        // Show tooltip
        tip.show(d,i);
      })
      .on("mouseout", function(d,i) {
        var self = d3.select(this);

        var line_opacity = this.parentNode.childNodes;
        d3.select(line_opacity[1])
          .style("opacity", "");

        // Highlight the name
        var index = self.attr('data-index');
        var nameElement = d3.select('#names-' + index);
        nameElement.style({
          opacity: 0.5,
          "font-weight": "normal",
          fill: configuration.textColor
        });

        self.attr("r", 4)
            .transition()
            .duration(750);

        tip.hide(d,i);
      })
      ;

  // Styling
  d3.select('body').style({
    "background-color": configuration.backgroundColor
  });

  d3.selectAll('.data_point').style({
    "stroke": configuration.backgroundColor,
    "stroke-width": "3px"
  });

  d3.selectAll('.line').style({
    "stroke-width": "9px"
  });

  d3.selectAll('.x.axis path').style({
    "stroke": configuration.domains.x.color
  });

  d3.selectAll('.x.axis .tick line').style({
    "stroke": configuration.domains.x.tickColor
  });

  d3.selectAll('.x.axis .tick text').style({
    "fill": configuration.domains.x.textColor
  });

  d3.selectAll('.y.axis path').style({
    "stroke": configuration.domains.y.color
  });

  d3.selectAll('.y.axis .tick line').style({
    "stroke": configuration.domains.y.tickColor
  });

  d3.selectAll('.y.axis .tick text').style({
    "fill": configuration.domains.y.textColor
  });
}
var graph = {
  // General configuration options
  general: {
    title: "Program Performance",
    titleColor: "#ccc",

    backgroundColor: "#444",
    textColor: "#ccc",

    domains: {
      x: {
        // The color of the axis
        color: "#222",
        tickColor: "#333",
        minorTickColor: "#383838",
        textColor: "#ccc",

        // The label for each value
        series: [{
          name: "fft"
        }, {
          name: "mmult"
        }, {
          name: "tar"
        }],

        // The overall label for the axis
        name: "Application"
      },
      y: {
        // The color of the axis
        color: "#222",
        tickColor: "#333",
        minorTickColor: "#3d3d3d",
        textColor: "#ccc",

        // The min/max values
        min: 0,
        max: 100,

        // Tick steps
        majorStep: 10, // Major steps get a text label
        minorStep: 2,  // Minor steps get just a tick

        // The overall label for the axis
        name: "% of cpu"
      }
    }
  },
  datapoints: {
    groups: [
      {
        // Color for this group
        color:       "hsl(120, 60%, 60%)",

        // Name for this group
        name:        "gcc",
      },
      {
        color:       "hsl(0, 60%, 60%)",
        name:        "clang clang clang",
      },
      {
        color:       "hsl(180, 60%, 60%)",
        name:        "msc",
      }
    ]
  }
};

var data = {
  datapoints: {
    groups: [{
      series: [80, 100, 40]
    }, {
      series: [23, 50,  66]
    }, {
      series: [33, 44,  54]
    }]
  }
};

// If the window resizes, redraw the graph to fit
window.onresize = function(event) {
  d3.select('svg').remove();
  d3.select('.d3-tip').remove();

  graph.width  = window.innerWidth  - 3;
  graph.height = window.innerHeight - 10;
  draw(graph, data);
};
window.onresize();

// Handle OCCAM events
window.addEventListener('message', function(event) {
  if (event.data.metadata !== undefined) {
    graph = event.data.metadata;
  }

  if (event.data.data !== undefined) {
    data = event.data.data;
  }

  window.onresize();
});
